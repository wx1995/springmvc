package com.ex.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import com.ex.model.ProductModel;
import com.ex.model.User;

// 转换为对象，用什么注解.  @Controller @Compontent
//  业务逻辑，注解
@Controller
@RequestMapping("/user/") // 根路径
public class UserController {

	// 编辑接口
	// @RequestMapping("edit)
	
//	GET							POST
// 地址栏把数据传过来			HTTP协议请求体传过来的
// 1. 数据量有限				无限
// 2. 不安全					安全
// 3. 速度快（2倍）					速度慢

	// CRUD
	//   CREATE 创建         POST
	//   READ   读取		GET
	//   UPDATE 更新 	PUT
	//   DELETE 删除		DELETE
	
	//  method=RequestMethod.POST 要求请求必须用post
	@RequestMapping(value="edit", method=RequestMethod.POST)
	// 说明返回的结果是 JSON 数据
	@ResponseBody
	public Map<String,Object> edit(
			// required=false 说明此字段是可选，如果不写的话，默认就是 必须得填
			@RequestParam(value="id",required=false) String id,
			@RequestParam(value="name",required=false) String name,
			@RequestParam(value="address",required=false) String address,
			HttpServletRequest request) {
		
		System.out.println("进入edit");
		System.out.println("id:" + id);
		System.out.println("name:" + name);
		System.out.println("address:" + address);
		
		// 返回的 字符串，是页面的地址
		// return "index.jsp";
		
		// Spring JSON的插件， pom包已经引入进来

		Map<String,Object> map = new HashMap<String,Object>();
		
		// 结果 ret为0，代表成功
		map.put("ret", 100);
		map.put("msg", "数据库更新失败！");
		
		return map;
	}
	
	
	// spring mvc 配置接口的路径
	@RequestMapping("show") // 根路径
	public String show() {
		System.out.println("进到用户的控制层了！！！！");
		
		// /index.jsp
		//  第1个斜杠  代表   web项目的webapp目录(项目的根目录)
		//  如果不加斜杠，就是找相当于当前控制器路径下面的文件
		
		return "index.jsp";
	}
	
	@RequestMapping("findUserByReq") // 根路径
	public String findUserByReq(
			// required=false 说明此字段是可选，如果不写的话，默认就是 必须得填
			@RequestParam(value="username",required=false) String username,
			@RequestParam(value="password",required=false) String password,
			HttpServletRequest request
			)  {
		System.out.println("进到findUserByReq！！！！");
		System.out.println("username:" + username);
		System.out.println("password:" + password);
		
		// 输出 username 到页面中
		request.setAttribute("username", username);
	
		return "findUser.jsp";
	}
	
	
	
	

	// {a} 说明是1个可变的变量
	@RequestMapping(value="/detail/{a}") // 根路径
	@ResponseBody
	public Map<String, Object> detail(
			@PathVariable(value="a") Integer id
			)  {
		System.out.println("id:" + id);
		
		
		ProductModel p = new ProductModel();
		
		p.setId(100);
		p.setName("强哥");
		p.setPrice(Double.POSITIVE_INFINITY);
		
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("ret", 0);
		map.put("data", p);
		
		
		return map;
	}
	
	

	// {a} 说明是1个可变的变量
	@RequestMapping(value="/detailtest/{a}-{b}") // 根路径
	@ResponseBody
	public Map<String, Object> detailTest(
			@PathVariable(value="a") Integer a,
			@PathVariable(value="b") Integer b
			)  {
		System.out.println("a:" + a);
		System.out.println("b:" + b);
		
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("ret", 0);
		map.put("data", "12321312312");
		
		
		return map;
	}
	

	//@RequestMapping(value="/test2/{a:[a-z]+}{b:\\d+}")
	
	//@RequestMapping(value="/test2/{a:[a-z]+@}{b:\\w+}{c:\\.[a-z]+}")

	//@RequestMapping(value="/test2/{a:[a-z]+@}{b:\\w+}")
	@RequestMapping(value="/test2/{c:\\.")
	@ResponseBody
	public Map<String, Object> test2(
			
			)  {
	
	
		System.out.println("进入匹配函数");
		
		
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("ret", 0);
		map.put("data", "12321312312");
		
		
		return map;
	}
	
	
	
	//@RequestMapping(value="/mail/{a:[a-z]+@}{b:\\w+}{c:\\.[a-z]+}")
	
	//@RequestMapping(value="/mail/{a:[a-z]+@}")
	//@RequestMapping(value="/mail/{a}")
	@RequestMapping(value="/mail")
	@ResponseBody
	public Map<String, Object> mail() {
		
		System.out.println("enter!");
		
		return null;
	}
	

	@RequestMapping(value="/cart")
	@ResponseBody
	public Map<String, Object> cart(HttpSession session) {
		
		// HttpSession session = request.getSession();
		
//		String username = (String)session.getAttribute("username");
//		
//		System.out.println("username:" + username);
		
		User user= (User)session.getAttribute("user");

		System.out.println("username:" + user.getName());
		System.out.println("user age:" + user.getAge());
		
		return null;
	}
	
	@RequestMapping(value="/login")
	@ResponseBody
	public Map<String, Object> login(HttpSession session) {
		
		
		// service到数据库中查找用户
		if (true) {
			
			User user = new User();
			
			user.setName("zhangsan");
			user.setAge(18);
			
			session.setAttribute("user", user);
			
			System.out.println("保存用户对象成功");
			
		}
		
		// 将用户的信息保存到 session 变量中
		//session.setAttribute("username", "zhangsan");
		
		return null;
	}
	
	

	@RequestMapping(value="/aaaa")
	public String aaaa() {
		return "redirect:http://www.baidu.com";
	}
	
}
